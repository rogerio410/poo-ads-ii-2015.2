package br.edu.ifpi.empresa.aplicacao;

import br.edu.ifpi.empresa.modelo.Funcionario;
import br.edu.ifpi.empresa.modelo.Gerente;

public class CalculaBonificacao {
	
	private double total;
	
	public void calcular(Funcionario f){
		this.total += f.getBonus();
	}
	
	public void showTotal(){
		System.out.println("Total Bonus : "+this.total);
	}

}
