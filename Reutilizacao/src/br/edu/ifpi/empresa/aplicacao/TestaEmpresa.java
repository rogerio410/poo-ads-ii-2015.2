package br.edu.ifpi.empresa.aplicacao;

import br.edu.ifpi.empresa.modelo.Diretor;
import br.edu.ifpi.empresa.modelo.Funcionario;
import br.edu.ifpi.empresa.modelo.Gerente;

public class TestaEmpresa {
	
	public static void main(String[] args) {
		
		Funcionario funcionario = new Funcionario();
		funcionario.setCpf(123);
		funcionario.setNome("Maria");
		funcionario.setSalario(3400);
		
		Gerente gerente = new Gerente();
		gerente.setCpf(124);
		gerente.setNome("Rogerio");
		gerente.setSalario(10000);
		gerente.setNivel(5);
		
		Diretor diretor = new Diretor();
		diretor.setNome("Rogerio Jr.");
		diretor.setSalario(35000);
		
		CalculaBonificacao calculaBonificacao = new CalculaBonificacao();
		calculaBonificacao.calcular(funcionario);
		calculaBonificacao.calcular(gerente);
		calculaBonificacao.calcular(diretor);
			
		funcionario.cracha();
		gerente.cracha();
		System.out.println(funcionario);
		System.out.println(gerente);
		
		calculaBonificacao.showTotal();
		
	}

}
