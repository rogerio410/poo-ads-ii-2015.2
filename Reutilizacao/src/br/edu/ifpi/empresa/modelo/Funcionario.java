package br.edu.ifpi.empresa.modelo;

public class Funcionario {
	
	private int cpf;
	private String nome;
	private double salario;
	
	public double getBonus(){
		return this.salario * 0.1;
	}
	
	public Funcionario() {
		// TODO Auto-generated constructor stub
	}
	
	public Funcionario(int cpf, String nome) {
		this.cpf = cpf;
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.nome+" Salario: "+this.salario+ " Bonus " +this.getBonus();
	}
	
	public final void cracha(){
		System.out.println("BTG: "+ this.getNome());
	}
	
	public int getCpf() {
		return cpf;
	}
	public void setCpf(int cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}
	
	

}
