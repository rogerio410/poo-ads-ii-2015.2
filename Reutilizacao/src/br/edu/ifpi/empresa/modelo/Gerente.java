package br.edu.ifpi.empresa.modelo;

public class Gerente extends Funcionario{
	
	private int nivel;
	
	@Override
	public String toString() {
		this.turno = "Tarde";
		return "Ger. "+super.toString() + " Nivel "+this.nivel;
	}
	
	@Override
	public double getBonus() {
		return this.getSalario()*0.3;
	}
	
	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	
	
}
