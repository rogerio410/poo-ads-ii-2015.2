class TestaConta{

	public static void main(String[] args) {
		Conta conta1 = new Conta();
		conta1.titular = "Rogerio Silva";
		conta1.numero = 123;
		conta1.depositar(8000);

		Conta conta2 = new Conta();
		conta2.titular = "Valeria Costa";
		conta2.numero = 124;
		conta2.depositar(10000);

		conta2.transferir(1000, conta1);

		System.out.println(conta1);
		System.out.println(conta2);
	}
	
}