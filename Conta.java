class Conta{
	private int numero;
	private String titular;
	private String cpf;
	private double saldo;

	public void setTitular(String titular){
		this.titular = titular;
	}

	public String getTitular(){
		return titular;
	}

	public boolean sacar(double valor){
		if (valor <= saldo){
			saldo -= valor;		
			return true;
		}else{
			System.out.println("Saldo insuficiente!");
			return false;
		}	
	}

	public void transferir(double valor, Conta contaDestino){
		if (sacar(valor)){
			contaDestino.depositar(valor);	
		}
	}

	public void depositar(double valor){
		saldo += valor;
	}

	public String toString(){
		return "Conta: " + numero + " Saldo: " + saldo; 
	}

}






